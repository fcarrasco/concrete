/*jslint node: true */
/*global require */

'use strict';

var jwt = require('jsonwebtoken');
var config = require('../config');

exports.validate = function (req, res, next) {
    var token = req.headers.authorization.split(' ')[1];
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err === 'TokenExpiredError') {
                return res.status(401).json({
                    messagem: 'Sessão Inválida'
                });
            } else if (err) {
                return res.status(401).json({
                    messagem: 'Não autorizado'
                });
            }
            req.decoded = decoded;
            req.token = token;
            next();
        });
    } else {
        return res.status(403).json({
            message: 'Não autorizado'
        });
    }
};
