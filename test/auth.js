/*jslint node: true */
/*global require, describe, it, before */

'use strict';

var app = require('../index');
var request = require('supertest')(app);
var expect = require('chai').expect;
var User = require('../models/user');

describe('Auth', function () {
    describe('Sign Up', function () {

        before(function (done) {
            // Truncate collection
            User.remove({}, function (err) {
                done();
            });
        });

        it('should create an user', function (done) {
            this.timeout(10000);
            var user = {
                nome: 'Nome do usuário',
                email: 'teste@teste.com.br',
                senha: '123456789',
                telefones: [
                    {
                        numero: '9999-9999',
                        ddd: '011'
                    },
                    {
                        numero: '98888-9999',
                        ddd: '011'
                    },
                ]
            };

            request.post("/signup")
                .type('json')
                .send(user)
                .set('Accept', 'application/json')
                .expect(200, done);
        });

        it('should get an error with another user with the same email', function (done) {
            var user = {
                nome: 'Nome do usuário 2',
                email: 'teste@teste.com.br',
                senha: '123456789',
                telefones: [
                    {
                        numero: '9999-9999',
                        ddd: '011'
                    }
                ]
            };

            request.post("/signup")
                .type('json')
                .send(user)
                .set('Accept', 'application/json')
                .expect(function (res) {
                    res.body.mensagem = 'E-mail já existente';
                })
                .expect(400, done);
        });
    });

    describe('Sign In', function () {

        it('should login', function (done) {
            this.timeout(10000);
            var user = {email: 'teste@teste.com.br', senha: '123456789'};
            request.post("/signin")
                .type('json')
                .send(user)
                .set('Accept', 'application/json')
                .expect(200, done);
        });

        it('should get an error without a json with email and senha', function (done) {
            request.post("/signin")
                .type('json')
                .send({})
                .set('Accept', 'application/json')
                .expect(function (res) {
                    res.body.mensagem = 'Usuário e/ou senha inválidos';
                })
                .expect(401, done);
        });
    });
});
