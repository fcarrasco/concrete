/*jslint node: true */
/*global require, describe, it, before */

'use strict';

var app = require('../index');
var request = require('supertest')(app);
var expect = require('chai').expect;
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../config');

var token = null;
var user_id = null;
var user_id2 = null;

describe('User', function () {
    before(function (done) {
        this.timeout(10000);

        var userLogin = {email: 'teste@teste.com.br', senha: '123456789'},
            newUser = {nome: 'Teste 2', email: 'teste2@teste.com.br', senha: '123456789'},
            user = new User(newUser);
        user.save(function (err, userDB) {
            user_id2 = userDB._id;
            request.post("/signin")
                .type('json')
                .send(userLogin)
                .end(function (err, res) {
                    if (!err) {
                        token = res.body.token;
                        user_id = res.body._id;
                    }
                    done();
                });
        });

    });

    it('should get an user with the valid token', function (done) {
        request.get('/user/' + user_id)
            .type('json')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer: ' + token)
            .expect(200, done);
    });

    it('should be no authorized if try to get a diferent user', function (done) {
        request.get('/user/' + user_id2)
            .type('json')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer: ' + token)
            .expect(401, done);
    });

    it('should be expired if the token have more then 30 min', function (done) {
        var expiredToken = jwt.sign({email: 'teste@teste.com.br'}, config.secret, {
            expiresIn: '0s'
        });
        request.get('/user/' + user_id)
            .type('json')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer: ' + expiredToken)
            .expect(401, done);
    });
});
