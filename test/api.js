/*jslint node: true */
/*global require, describe, it */

'use strict';

var app = require('../index');
var request = require('supertest')(app);
var expect = require('chai').expect;

describe('API', function () {

    it('should return 200 on /', function (done) {
        request.get("/")
            .expect('Content-type', /json/)
            .expect(200, done);
    });

    it('should return 404 on a wrong url', function (done) {
        request.get("/url-that-does-not-exists")
            .expect('Content-type', /json/)
            .expect(404, done);
    });
});
