/*jslint node: true */
/*global require, module, console */

'use strict';

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

module.exports = app;

var port = process.env.PORT || 5000;

var mongoose = require('mongoose');
var config = require('./config');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({}));

app.use(require('./controllers'));

function listen() {
    if (app.get('env') === 'test') {
        return;
    }
    app.listen(port);
    console.log('app started on port ' + port);
}

function connect() {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    return mongoose.connect(config.database, options).connection;
}

connect()
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);
