/*jslint node: true */
/*global require, module */

var express = require('express');
var app = express();

var config = {
    'development': {
        secret: 'app-secret-for-concrete-test',
        database: 'mongodb://localhost/concrete-dev'
    },
    'test': {
        secret: 'app-secret-for-concrete-test',
        database: 'mongodb://localhost/concrete-test'
    }
};

module.exports = config[app.get('env')];
