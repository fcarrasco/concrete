/*jslint node: true */
/*global require, module, console */

'use strict';

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var config = require('../config');

function getToken(email) {
    var token = jwt.sign({email: email}, config.secret, {
        expiresIn: '30m'
    });
    return token;
}

router.post('/signup', function (req, res) {
    var data = req.body;
    User.findOne({email: data.email}, function (err, dbUser) {
        if (err) {
            throw err;
        }
        if (!dbUser) {
            data.senha = bcrypt.hashSync(data.senha);
            data.token = getToken(data.email);
            var user = new User(data);
            user.save(function (err, user) {
                if (!err) {
                    res.status(200).json(user);
                } else {
                    throw err;
                }
            });
        } else {
            res.status(400).json({
                mensagem: 'E-mail já existente'
            });
        }
    });
});

router.post('/signin', function (req, res) {
    var email = req.body.email,
        password = req.body.senha;
    User.findOne(
        {email: email},
        function (err, user) {
            if (err) {
                throw err;
            }
            if (user) {
                var hash = user.senha;
                if (bcrypt.compareSync(password, hash)) {
                    user.token = getToken(email);
                    user.save(function (err) {
                        res.json(user);
                    });
                } else {
                    res.status(401).json({
                        mensagem: 'Usuário e/ou senha inválidos'
                    });
                }
            } else {
                res.status(401).json({
                    mensagem: 'Usuário e/ou senha inválidos'
                });
            }
        }
    );
});

module.exports = router;
