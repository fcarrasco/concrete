/*jslint node: true */
/*global require, module, console, o */

'use strict';

var express = require('express');
var router = express.Router();

router.use('/', require('./auth'));
router.use('/', require('./user'));

router.get('/', function (req, res) {
    res.status(200).json({messagem: 'API test Concrete'});
});

router.use(function (req, res, next) {
    res.status(404);
    res.send({mensagem: '404: Endereço não encontrado'});
    return;
});

module.exports = router;
