/*jslint node: true */
/*global require, module, console */

'use strict';

var express = require('express');
var router = express.Router();
var auth = require('../middleware/auth');
var User = require('../models/user');

router.get('/user/:id', auth.validate, function (req, res) {
    var id = req.params.id;
    User.findOne({'_id': id}, function (err, user) {
        if (err) {
            throw err;
        }
        if (user) {
            if (user.email === req.decoded.email) {
                res.status(200).json(user);
            } else {
                res.status(401).json('Não Autorizado');
            }
        } else {
            res.status(400).json('Usuário não encontrado');
        }
    });
});

module.exports = router;
