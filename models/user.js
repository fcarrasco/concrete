/*jslint node: true */
/*global require, module, console */

'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var nodeuuid = require('node-uuid');

var userSchema = new Schema({
    '_id': {
        type: String,
        default: function genUUID() {
            return nodeuuid.v1();
        }
    },
    nome: String,
    email: String,
    senha: String,
    telefones: { type: Array, default: [] },
    data_criacao: { type: Date, default: Date.now },
    data_atualizacao: { type: Date, default: Date.now },
    ultimo_login: { type: Date, default: Date.now },
    token: String
});

module.exports = mongoose.model('User', userSchema);
